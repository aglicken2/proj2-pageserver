from flask import Flask
from flask import render_template
from flask import abort
from flask import request
import os


app = Flask(__name__)

@app.route("/<path:link>")
def hello(link):
    # print("This is path {}\n".format(path))
    # print(request.path)
    # print("//" in link)
    
    link = str(link)
    
    # print("$$$ This is the link {}$$$\n".format(link))
    
    parts = link.split('.')
    
    # print("****Here are parts*** {}".format(parts))
    # print("****Here are parts[0]*** {}".format(parts[0]))
    
    endURL = os.path.basename(parts[-1])
    file_path = os.path.join("templates/" + link)
    
    if (endURL != "html") and (endURL != "css"):
        # return("403 error"), 403
        abort(403)
    
    elif "~" in link or "//" in link or ".." in link:  
        # return("403 error"), 403
        abort(403)
       
    elif os.path.exists(file_path) == False:  
        # return("404 error"), 404
        abort(404)

    else:
        return render_template(link)

         
@app.errorhandler(403)
def error_forbidden(error):
    return render_template('403.html'), 403         
    
@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
    # app.run(debug=True)
