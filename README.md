A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

# README #

A project for CIS 322, introduction to software engineering, at the University of Oregon. This project is
an introduction to flask apps and docker usage. Completed by Anne Glickenhaus, aglicken@uoregon.edu.